"use strict";
$(document).ready(function() {
    // Vùng 1: Khai báo biến toàn cục
    var gComboSize = [];
    var gPizzaType = "";
    var gPercentDiscount = 0;
    var gPriceAfterDiscount = 0;
    var gDrinkSelectedText = "";
    var gDrinkSelectedValue = "";
    var gAllInfoOrder = [];

    const gBASE_DRINK_LIST = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
    const gBASE_VOUCHER = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
    const gBASE_ORDER = "http://42.115.221.44:8080/devcamp-pizza365/orders";

    // Vùng 2: Vùng gán
    onPageLoading();

    $('#select-drink').on('change', function() {
        gDrinkSelectedText = $('#select-drink option:selected').text();
        gDrinkSelectedValue = $('#select-drink option:selected').val();
        console.log(gDrinkSelectedText);
    });

    $('#btn-size-S').on('click', onBtnGetSizeSClick);
    $('#btn-size-M').on('click', onBtnGetSizeMClick);
    $('#btn-size-L').on('click', onBtnGetSizeLClick);

    $('#btn-pizza-seafood').on('click', onBtnGetPizzaSeafoodClick);
    $('#btn-pizza-hawaii').on('click', onBtnGetPizzaHawaiiClick);
    $('#btn-pizza-bacon').on('click', onBtnGetPizzaBaconClick);

    $('#btn-send').on('click', onBtnSendOrderClick);
    $('#btn-modal-create-order').on('click', onBtnCreateOrderClick);

    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi load trang
    function onPageLoading() {
        "use strict";
        console.log("Load trang");
        callApiGetDrinkList();
    }

    // Hàm xử lý khi click chọn size S
    function onBtnGetSizeSClick() {
        "use strict";
        console.log("Chọn Size S");
        var vComboSizeS = {
            menuName: "S",
            duongKinhCM: 20,
            suonNuong: 2,
            saladGr: 200,
            drink: 2,
            priceVND: 150000
        };
        gComboSize = vComboSizeS;
        console.log(gComboSize);
        changeColorButtonSizeCombo();
    }

    // Hàm xử lý khi click chọn size M
    function onBtnGetSizeMClick() {
        "use strict";
        console.log("Chọn Size M");
        var vComboSizeM = {
            menuName: "M",
            duongKinhCM: 25,
            suonNuong: 4,
            saladGr: 300,
            drink: 3,
            priceVND: 200000
        };
        gComboSize = vComboSizeM;
        console.log(gComboSize);
        changeColorButtonSizeCombo();
    }

    // Hàm xử lý khi click chọn size L
    function onBtnGetSizeLClick() {
        "use strict";
        console.log("Chọn Size L");
        var vComboSizeL = {
            menuName: "L",
            duongKinhCM: 30,
            suonNuong: 8,
            saladGr: 500,
            drink: 4,
            priceVND: 250000
        };
        gComboSize = vComboSizeL;
        console.log(gComboSize);
        changeColorButtonSizeCombo();
    }

    // Hàm xử lý khi click chọn pizza seafood
    function onBtnGetPizzaSeafoodClick() {
        "use strict";
        gPizzaType = "Pizza Seafood";
        console.log("Chọn: " + gPizzaType);
        changeColorButtonPizzaType();
    }

    // Hàm xử lý khi click chọn pizza hawaii
    function onBtnGetPizzaHawaiiClick() {
        "use strict";
        gPizzaType = "Pizza Hawaii";
        console.log("Chọn: " + gPizzaType);
        changeColorButtonPizzaType();
    }

    // Hàm xử lý khi click chọn pizza bacon
    function onBtnGetPizzaBaconClick() {
        "use strict";
        gPizzaType = "Pizza Bacon";
        console.log("Chọn: " + gPizzaType);
        changeColorButtonPizzaType();
    }

    // Hàm xử lý khi click gửi
    function onBtnSendOrderClick() {
        "use strict";
        //B1: Tạo đối tượng và lưu trữ dữ liệu từ form
        //console.log(gDrinkSelected);
        var vOrderObjRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        };
        getInfoFromForm(vOrderObjRequest);
        gAllInfoOrder = vOrderObjRequest;
        //console.log(vInfoObj);
        //B2: Validate
        var vIsCheckInfo = validateInfoOrder(vOrderObjRequest);
        console.log(vIsCheckInfo);
        if (vIsCheckInfo == true) {
            // B3: Hiển thị modal và thông tin vào modal
            checkDiscountVoucher(vOrderObjRequest.idVourcher);
            loadDataToModalInfoOrder(vOrderObjRequest);
            $('#modal-info-order').modal('show');
        }
    }

    // Hàm xử lý khi click tạo đơn trên modal
    function onBtnCreateOrderClick() {
        "use strict";
        //console.log(gAllInfoOrder);
        callApiPostOrder();
    }

    // Vùng 4: Khai báo hàm dùng chung
    // Hàm đổi màu button combo size
    function changeColorButtonSizeCombo() {
        "use strict";
        if (gComboSize.menuName === "S") {
            $('#btn-size-S').removeClass("btn-warning")
                            .addClass("btn-info");
            $('#btn-size-M').removeClass("btn-info")
                            .addClass("btn-warning");
            $('#btn-size-L').removeClass("btn-info")
                            .addClass("btn-warning");
        }
        if (gComboSize.menuName === "M") {
            $('#btn-size-M').removeClass("btn-warning")
                            .addClass("btn-info");
            $('#btn-size-S').removeClass("btn-info")
                            .addClass("btn-warning");
            $('#btn-size-L').removeClass("btn-info")
                            .addClass("btn-warning");
        }
        if (gComboSize.menuName === "L") {
            $('#btn-size-L').removeClass("btn-warning")
                            .addClass("btn-info");
            $('#btn-size-S').removeClass("btn-info")
                            .addClass("btn-warning");
            $('#btn-size-M').removeClass("btn-info")
                            .addClass("btn-warning");
        }
    }

    // Hàm đổi màu button Pizza type
    function changeColorButtonPizzaType() {
        "use strict";
        if (gPizzaType === "Pizza Seafood") {
            $('#btn-pizza-seafood').removeClass("btn-warning")
                            .addClass("btn-info");
            $('#btn-pizza-hawaii').removeClass("btn-info")
                            .addClass("btn-warning");
            $('#btn-pizza-bacon').removeClass("btn-info")
                            .addClass("btn-warning");
        }
        if (gPizzaType === "Pizza Hawaii") {
            $('#btn-pizza-hawaii').removeClass("btn-warning")
                            .addClass("btn-info");
            $('#btn-pizza-seafood').removeClass("btn-info")
                            .addClass("btn-warning");
            $('#btn-pizza-bacon').removeClass("btn-info")
                            .addClass("btn-warning");
        }
        if (gPizzaType === "Pizza Bacon") {
            $('#btn-pizza-bacon').removeClass("btn-warning")
                            .addClass("btn-info");
            $('#btn-pizza-seafood').removeClass("btn-info")
                            .addClass("btn-warning");
            $('#btn-pizza-hawaii').removeClass("btn-info")
                            .addClass("btn-warning");
        }
    }

    // Hàm gọi api lấy drink list
    function callApiGetDrinkList() {
        "use strict";
        $.ajax({
            url: gBASE_DRINK_LIST,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function(result) {
                console.log(result);
                loadDrinkListToSelectDrink(result);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }

    // Hàm add select drink
    function loadDrinkListToSelectDrink(paramDrinkList) {
        "use strict";
        for (var bI = 0; bI < paramDrinkList.length; bI++) {
            $('#select-drink').append($('<option>', {
                value: paramDrinkList[bI].maNuocUong,
                text: paramDrinkList[bI].tenNuocUong
            }));
        }
    }

    // Hàm lấy thông tin từ form gửi đơn hàng
    function getInfoFromForm(paramObject) {
        "use strict";
        paramObject.hoTen = $.trim($('#inp-fullname').val());
        paramObject.email = $.trim($('#inp-email').val());
        paramObject.soDienThoai = $.trim($('#inp-phone').val());
        paramObject.diaChi = $.trim($('#inp-address').val());
        paramObject.idVourcher = $.trim($('#inp-voucher-id').val());
        paramObject.loiNhan = $.trim($('#inp-message').val());

        paramObject.kichCo = gComboSize.menuName;
        paramObject.duongKinh = gComboSize.duongKinhCM;
        paramObject.suon = gComboSize.suonNuong;
        paramObject.salad = gComboSize.saladGr;
        paramObject.loaiPizza = gPizzaType;
        paramObject.idLoaiNuocUong = gDrinkSelectedValue;
        paramObject.soLuongNuoc = gComboSize.drink;
        paramObject.thanhTien = gComboSize.priceVND;
    }

    // Hàm validate info order
    function validateInfoOrder(paramObject) {
        "use strict";
        var vEmailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (gComboSize.menuName === undefined) {
            alert("Chưa chọn Combo");
            return false;
        }
        if (gPizzaType == "") {
            alert("Chưa chọn loại Pizza");
            return false;
        }
        if ($('#select-drink :selected').val() == "0") {
            alert("Chưa chọn Drink");
            return false;
        }
        if (paramObject.fullname == "") {
            alert("Chưa điền Họ tên");
            return false;
        }
        if (paramObject.email == "") {
            alert("Chưa điền Email");
            return false;
        }
        if (!paramObject.email.match(vEmailFormat)) {
            alert("Email không hợp lệ");
            return false;
        }
        if (paramObject.phone == "") {
            alert("Chưa điền Số điện thoại");
            return false;
        }
        if (paramObject.address == "") {
            alert("Chưa điền Địa chỉ");
            return false;
        }
        return true;
    }

    // Hàm show thông tin lên modal info order
    function loadDataToModalInfoOrder(paramObject) {
        "use strict";
        var vInfoDetailOrder = "Xác nhận: " +
                                paramObject.hoTen + ", " +
                                paramObject.soDienThoai + ", " +
                                paramObject.diaChi + "\n" +
                                "Menu: " + gComboSize.menuName + ", " +
                                "sườn nướng: " + gComboSize.suonNuong + " cây, " +
                                "salad: " + gComboSize.saladGr + " gr, " +
                                "nước ngọt: " + gDrinkSelectedText + ", " +
                                "số lượng nước: " + gComboSize.drink + "\n" +
                                "Loại Pizza: " + gPizzaType + ", " +
                                "Giá: " + gComboSize.priceVND + " VND, " +
                                "Mã giảm giá: " + paramObject.idVourcher + "\n" +
                                "Phải thanh toán: " + gPriceAfterDiscount + "VND (Giảm giá: " + gPercentDiscount + "%)";
        $('#inp-modal-fullname').val(paramObject.hoTen);
        $('#inp-modal-phone').val(paramObject.soDienThoai);
        $('#inp-modal-address').val(paramObject.diaChi);
        $('#inp-modal-message').val(paramObject.loiNhan);
        $('#inp-modal-voucher-id').val(paramObject.idVourcher);
        $('#txt-modal-info-detail').val(vInfoDetailOrder);
    }

    // Hàm check voucher
    function checkDiscountVoucher(paramVoucherId) {
        "use strict";
        if (paramVoucherId == "") {
            gPercentDiscount = 0;
            gPriceAfterDiscount = gComboSize.priceVND*(1-(gPercentDiscount/100));
            console.log("Mã voucher rỗng!!!");
          }
          else if (paramVoucherId != null) {
            callApiGetVoucherList(paramVoucherId); 
          }
    }

    // Hàm call api lấy voucher list
    function callApiGetVoucherList(paramVoucherId) {
        "use strict";
        $.ajax({
            url: gBASE_VOUCHER + paramVoucherId,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function(result) {
                console.log(result);
                gPercentDiscount = result.phanTramGiamGia;
                console.log(gPercentDiscount);
                gPriceAfterDiscount = gComboSize.priceVND*(1-(gPercentDiscount/100));
            },
            error: function(error) {
                console.log(error.responseText);
                gPercentDiscount = 0;
                console.log("Voucher ID không có");
                gPriceAfterDiscount = gComboSize.priceVND*(1-(gPercentDiscount/100));
            }
        });
    }

    // Hàm call api post order
    function callApiPostOrder() {
        "use strict";
        $.ajax({
            url: gBASE_ORDER,
            type: 'POST',
            data: JSON.stringify(gAllInfoOrder),
            dataType:'json',
            contentType: "application/json;charset=UTF-8",
            success: function(result) {
                //alert("Tạo đơn thành công: " + Object.entries(result));
                console.log(result);
                $('#modal-info-order').modal('hide');
                $('#modal-order-id').modal('show');
                $('#inp-modal-order-id').val(result.orderId);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }

});